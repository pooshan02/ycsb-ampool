/**
 * 
 */
package com.yahoo.ycsb.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yahoo.ycsb.ByteArrayByteIterator;
import com.yahoo.ycsb.ByteIterator;
import com.yahoo.ycsb.DB;
import com.yahoo.ycsb.DBException;
import com.yahoo.ycsb.Status;
import com.yahoo.ycsb.workloads.CoreWorkload;

import io.ampool.client.AmpoolClient;
import io.ampool.conf.Constants;
import io.ampool.monarch.table.Bytes;
import io.ampool.monarch.table.Cell;
import io.ampool.monarch.table.Delete;
import io.ampool.monarch.table.Get;
import io.ampool.monarch.table.MTable;
import io.ampool.monarch.table.MTableDescriptor;
import io.ampool.monarch.table.Put;
import io.ampool.monarch.table.Row;
import io.ampool.monarch.table.Scan;
import io.ampool.monarch.table.Scanner;
import io.ampool.monarch.types.BasicTypes;

/**
 * @author pooshans
 *
 */
public class AmpoolDbClient extends DB {

	private static final Logger LOG = LoggerFactory.getLogger(AmpoolDbClient.class);
	private static AmpoolClient client;
	private static MTable mTable;

	@Override
	public void init() throws DBException {
		LOG.info("Intialization...");
		String locatorHost = getProperties().getProperty("locator");
		int locatorPort = Integer.parseInt(getProperties().getProperty("port"));
		String tableName = getProperties().getProperty("tableName");
		Properties props = new Properties();
		props.setProperty(Constants.MClientCacheconfig.MONARCH_CLIENT_LOG, "/tmp/" + tableName + ".log");
		client = new AmpoolClient(locatorHost, locatorPort, props);
		initClient(tableName, getProperties());
		LOG.info("Completed.");
	}

	@Override
	public Status delete(String table, String key) {
		LOG.info("Delete");
		try {
			Delete delete = new Delete(Bytes.toBytes(key));
			mTable.delete(delete);
			LOG.info("Completed");
			return Status.OK;
		} catch (Exception ex) {
			return Status.ERROR;
		}
	}

	@Override
	public Status insert(String table, String key, Map<String, ByteIterator> values) {
		LOG.info("Insertion started...");
		try {
			Put p = new Put(Bytes.toBytes(key));
			for (Map.Entry<String, ByteIterator> entry : values.entrySet()) {
				byte[] bValue = entry.getValue().toArray();
				byte[] bKey = Bytes.toBytes(entry.getKey());
				p.addColumn(bKey, bValue);
			}
			mTable.put(p);
			LOG.info("Completed");
			return Status.OK;
		} catch (Exception ex) {
			return Status.ERROR;
		}
	}

	@Override
	public Status read(String table, String key, Set<String> fields, Map<String, ByteIterator> result) {
		LOG.info("Reading started...");
		try {
			Get g = new Get(Bytes.toBytes(key));
			for (String field : fields) {
				g.addColumn(Bytes.toBytes(field));
			}
			Row row = mTable.get(g);
			for (Cell cell : row.getCells()) {
				result.put(Bytes.toString(cell.getColumnName()),
						new ByteArrayByteIterator((byte[]) cell.getColumnValue()));
			}
			LOG.info("Completed.");
			return Status.OK;
		} catch (Exception ex) {
			return Status.ERROR;
		}
	}

	@Override
	public Status scan(String table, String startkey, int recordcount, Set<String> fields,
			Vector<HashMap<String, ByteIterator>> result) {
		LOG.info("Scanning started.");
		try {
			Scan scan = new Scan();
			scan.setStartRow(Bytes.toBytes(startkey));
			scan.setBatchSize(recordcount);
			scan.enableBatchMode(); // enable paging/batched mode
			scan.setReturnKeysFlag(true);
			for (String field : fields) {
				scan.addColumn(Bytes.toBytes(field));
			}
			Scanner scanner = mTable.getScanner(scan);
			Row[] rows = scanner.nextBatch();
			System.out.println("Paging through results");
			while (rows.length > 0) {
				for (int i = 0; i < rows.length; i++) {
					HashMap<String, ByteIterator> rowResult = new HashMap<String, ByteIterator>();
					for (Cell cell : rows[i].getCells()) {
						rowResult.put(Bytes.toString(cell.getColumnName()),
								new ByteArrayByteIterator((byte[]) cell.getColumnValue()));
					}
					result.add(rowResult);
				}
				String input = System.console().readLine();
				if (input.startsWith("e") || input.startsWith("E")) {
					System.out.println("Ending scan.");
					break;
				}
				rows = scanner.nextBatch();
			}
			LOG.info("Completed.");
			return Status.OK;
		} catch (Exception ex) {
			return Status.ERROR;
		}
	}

	@Override
	public Status update(String table, String key, Map<String, ByteIterator> values) {
		LOG.info("Update started..");
		//return insert(table, key, values);
		return Status.OK;
	}

	private static int getIntFromProp(Properties prop, String propName, int defaultValue) throws DBException {
		String intStr = prop.getProperty(propName);
		if (intStr == null) {
			return defaultValue;
		} else {
			try {
				return Integer.valueOf(intStr);
			} catch (NumberFormatException ex) {
				throw new DBException("Provided number for " + propName + " isn't a valid integer");
			}
		}
	}

	private static synchronized void initClient(String tableName, Properties prop) throws DBException {
		if (mTable != null) {
			return;
		} else if (client.getAdmin().existsMTable(tableName)) {
			return;
		}

		int fieldCount = getIntFromProp(prop, CoreWorkload.FIELD_COUNT_PROPERTY,
				Integer.parseInt(CoreWorkload.FIELD_COUNT_PROPERTY_DEFAULT));

		MTableDescriptor td = new MTableDescriptor();
		for (int i = 0; i < fieldCount; i++) {
			td.addColumn(Bytes.toBytes("field" + i), BasicTypes.STRING);
		}
		mTable = client.getAdmin().createMTable(tableName, td);

	}

}